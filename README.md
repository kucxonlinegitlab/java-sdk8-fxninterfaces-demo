GitLab Project Name:
java-sdk8-fxninterfaces-demo

Author:
Karen Urate

Date:
2017-May-28

Description:
This project explores some new features of JAVA 8's functional interfaces such as the Supplier, Function, Consumer, and Predicate. It also provides a good introduction to JAVA 8's List, Stream, Map, and DateTimeFormatter.

This project was created using IntelliJ IDEA 2016.2.5 IDE. 
You may open and use this project in any IDE that compiles JAVA 8.
You may copy and distribute this project for personal and educational purposes.