package fxninterfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**

 Created by Karen Urate on 2017-05-27.

 Description: A Predicate represents an argument that returns a boolean value and is very commonly used for filtering or matching conditions.

 Syntax:
    Predicate<T> predicate;

 Return Type:
    boolean

 How many parameters?
    1

 Abstract Method:
    get()

 */
public class PredicateDemo {

    private static List<String> RANDOM_COUNTRIES = Arrays.asList("United Arab Emirates","Bangladesh","Canada","Denmark","Egypt","France","United States");

    public static void main(String[] args) {

        System.out.println("Example of Predicate With List and Stream \n");
        Predicate<List<String>> countryNamesSize = c -> c.stream().count() < 10;
        Predicate<List<String>> countryNamesStartsWithU = c ->
        {
            return c.stream().anyMatch(name -> name.startsWith("U"));
        };

        Predicate<List<String>> combinedPredicates = countryNamesSize.and(countryNamesStartsWithU);
        System.out.println("PREDICATE #1 [c -> c.stream().count() < 10]: "+countryNamesSize.test(RANDOM_COUNTRIES));
        System.out.println("PREDICATE #2 [c.stream().anyMatch(name -> name.startsWith(\"U\")]: "+countryNamesStartsWithU.test(RANDOM_COUNTRIES));
        System.out.println("COMBINATION OF PREDICATES 1 & 2: "+combinedPredicates.test(RANDOM_COUNTRIES));

    }

}
