package fxninterfaces;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;

/**

 Created by Karen Urate on 2017-05-27.

 Description:  A Supplier generates or supplies values without taking parameters or inputs.

 Syntax:
    Supplier<T> supplier;

 Return Type:
    T

 How many parameters?
    0

 Abstract Method:
    get()

 */
public class SupplierDemo {

    private static LocalDate currentDate;
    private static DateTimeFormatter formatter;

    public static void main(String[] args){

        System.out.println("\n*** TODAY ***\n");
        Supplier<LocalDate> today = () -> LocalDate.of(2017,5,27);
        currentDate = today.get();

        System.out.println(currentDate.toString());       // 2017-05-27
        System.out.println(currentDate.getChronology());  // ISO
        System.out.println(currentDate.getDayOfMonth());  // 27
        System.out.println(currentDate.getDayOfWeek());  // SATURDAY
        System.out.println(currentDate.getDayOfYear());  // 147
        System.out.println(currentDate.atStartOfDay());  // 2017-05-27T00:00
        System.out.println(currentDate.atStartOfDay( ZoneId.of("UTC") ));  // 2017-05-27T00:00Z[UTC]
        System.out.println(currentDate.getEra());  // CE
        System.out.println(currentDate.getMonthValue());  // 5
        System.out.println(currentDate.isLeapYear());  // false
        System.out.println(currentDate.isAfter(LocalDate.now()));  // false
        System.out.println(currentDate.isBefore(LocalDate.now()));  // false

        System.out.println("\n*** TOMORROW ***\n");
        Supplier<LocalDate> tomorrow = () -> { return LocalDate.of(2017,05,27).plusDays(1); };
        Supplier<DateTimeFormatter> theFormatter1 = () -> formatter.ISO_DATE;
        Supplier<DateTimeFormatter> theFormatter2 = () -> formatter.BASIC_ISO_DATE;
        Supplier<DateTimeFormatter> theFormatter3 = () -> formatter.ISO_LOCAL_DATE;
        currentDate = tomorrow.get();

        System.out.println(currentDate.toString());       // 2017-05-28
        System.out.println(currentDate.format(theFormatter1.get()));
        System.out.println(currentDate.format(theFormatter2.get()));
        System.out.println(currentDate.format(theFormatter3.get()));

    }


}
