package fxninterfaces;

import java.time.*;
import java.util.*;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

/**

 Created by Karen Urate on 2017-05-27.

 Description:  A Consumer takes in a single parameter, and manipulates it, but does not return anything.

 Syntax:
    Consumer<T> consumer;

 Return Type:
    void

 How many parameters?
    1

 Abstract Method:
    accept()

 */
public class ConsumerDemo {

    private static LocalDate CURRENT_DATE = LocalDate.now(Clock.systemDefaultZone());
    private static ZonedDateTime ZDT_DATE = ZonedDateTime.now();
    private static LocalDateTime LOCALDT = LocalDateTime.of(2017,05,28,14,27);
    private static String PATTERN_1 = "EEEE, yyyy-MM-dd";
    private static String PATTERN_2 = "EEEE, yyyy-MM-dd hh:mm a";
    private static String PATTERN_3 = "EEEE, yyyy-MM-dd hh:mm a OOOO VV";
    private static String PATTERN_4 = "yyyy-MMM-dd hh:mm a ZZZZ";
    private static DateTimeFormatter FORMATTER_1 = DateTimeFormatter.ISO_DATE;
    private static DateTimeFormatter FORMATTER_2 = DateTimeFormatter.ofPattern(PATTERN_1,Locale.CANADA);
    private static DateTimeFormatter FORMATTER_3 = DateTimeFormatter.ofPattern(PATTERN_2,Locale.CANADA);
    private static DateTimeFormatter FORMATTER_4 = DateTimeFormatter.RFC_1123_DATE_TIME;
    private static DateTimeFormatter FORMATTER_5 = DateTimeFormatter.ofPattern(PATTERN_3,Locale.CANADA);
    private static DateTimeFormatter FORMATTER_6 = DateTimeFormatter.ofPattern(PATTERN_4,Locale.CANADA);
    private static DateTimeFormatter FORMATTER_7 = DateTimeFormatter.ofPattern("D",Locale.CANADA);

    public static void main(String[] args){

        Consumer<DateTimeFormatter> formatDateToday01 = d -> {
            String dateToday = d.format(LOCALDT);
            System.out.println("Pattern 1 - (EEEE, yyyy-MM-dd hh:mm a): "+dateToday);
        };
        formatDateToday01.accept(FORMATTER_3);

        Consumer<DateTimeFormatter> formatDateToday02 = d -> {
            System.out.println("Pattern 2 - (EEEE, yyyy-MM-dd): "+d.format(CURRENT_DATE));
        };
        formatDateToday02.accept(FORMATTER_2);

        Consumer<DateTimeFormatter> formatDateToday03 = d -> { System.out.println("Pattern 3 - (EEEE, yyyy-MM-dd hh:mm a OOOO VV): "+d.format(ZDT_DATE)); };
        formatDateToday03.accept(FORMATTER_5);

        Consumer<ZonedDateTime> formatDateToday04 = d -> {
            System.out.println("Pattern 4 - (ISO_DATE): "+FORMATTER_1.format(d));
            System.out.println("Pattern 5 - (RFC_1123_DATE_TIME): "+FORMATTER_4.format(d));
            System.out.println("Pattern 6 - (yyyy-MMM-dd hh:mm a ZZZZ): "+FORMATTER_6.format(d));
            System.out.println("Pattern 7 - (D): "+FORMATTER_7.format(d));
        };
        formatDateToday04.accept(ZDT_DATE);
    }
    
}