package fxninterfaces;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**

 Created by Karen Urate on 2017-05-27.

 Description:  A Function takes in a single parameter of a certain type, operates on the parameter, and returns an object of a different type

 Syntax:
    Function<T,R> function;

 Return Type:
    R

 How many parameters?
    1

 Abstract Method:
    apply()

 */
public class FunctionDemo {

    private static List<String> RANDOM_COUNTRIES = Arrays.asList("United Arab Emirates","Bangladesh","Canada","Denmark","Egypt","France","United States");

    public static void main(String[] args) {

        System.out.println("Example of Function With List, Stream, and Map \n\n");
        List<String> countryNames = RANDOM_COUNTRIES.stream()
                .filter(c -> c.length()<= 13)
                .collect(Collectors.toList());

        List<String> countryStartsWithU = RANDOM_COUNTRIES.stream()
                .filter(c -> c.startsWith("U"))
                .collect(Collectors.toList());

        Function<String, Map> countryLocation = c -> {

            Map<String, double[]> location = getLocation(c);
            return location;

        };

        System.out.println("***Country Names With Length of 13 or Less***");
        countryNames.forEach(

                c -> {
                    Map<String, double[]> location = countryLocation.apply(c);
                    location.forEach(
                            (name,point) -> {
                                String found = name+": ";
                                for(double xy: point){
                                    found += xy;
                                    found += ", ";
                                }
                                System.out.println(found.substring(0,found.length()-2));
                            }
                    );
                }

        );

        System.out.println("\n***Country Names That Start With U***");
        countryStartsWithU.forEach(

                c -> {
                    Map<String, double[]> location = countryLocation.apply(c);
                    location.forEach(
                            (name,point) -> {
                                String found = name+" ("+c+")"+": ";
                                for(double xy: point){
                                    found += xy;
                                    found += ", ";
                                }
                                System.out.println(found.substring(0,found.length()-2));
                            }
                    );
                }

        );

        System.out.println("\n\nSimple Example of Function Using String & Integer");
        Function<String, Integer> f1 = String::length;
        Function<String, Integer> f2 = x -> x.length();

        System.out.println(f1.apply("super")); // 5
        System.out.println(f2.apply("fun")); // 3

    }

    public static Map getLocation(String country) {

        Map<String,double[]> location = new HashMap<>();

        switch(country){

            case "United Arab Emirates":
            {
                double[] point = {23.424076,53.847818};
                location.put("AE",point);
                break;
            }
            case "Bangladesh":
            {
                double[] point = {23.684994,90.356331};
                location.put("BD",point);
                break;
            }
            case "Canada":
            {
                double[] point = {56.130366,-106.346771};
                location.put("CA",point);
                break;
            }
            case "Denmark":
            {
                double[] point = {56.26392,9.501785};
                location.put("DK",point);
                break;
            }
            case "Egypt":
            {
                double[] point = {26.820553,30.802498};
                location.put("EG",point);
                break;
            }
            case "France":
            {
                double[] point = {46.227638,2.213749};
                location.put("FR",point);
                break;
            }
            default:
            {
                double[] point = {37.09024,-95.712891};
                location.put("US",point);
            }

        }//end switch

        return location;
    }
}
